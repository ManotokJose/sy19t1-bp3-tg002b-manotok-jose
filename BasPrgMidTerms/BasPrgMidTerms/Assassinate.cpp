#include "Assassinate.h"
#include <iostream>

using namespace std;

Assassinate::Assassinate()
{
	skillName = "Assassinate";
	costMP = 40;
}


Assassinate::~Assassinate()
{
}

void Assassinate::classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst)
{
	int damage;
	int unitNumber = 0;

	for (int i = 0; i < unitDefense.size(); i++)
	{
		if (unitDefense[unitNumber]->getHealth() <= 0)
		{
			unitNumber++;
		}

		if (unitDefense[i]->checkIfDead() == false)
		{
			if (unitDefense[i]->getHealth() < unitDefense[unitNumber]->getHealth())
			{
				unitNumber = i;
			}
		}
	}

	if (unitDefense[unitNumber]->getClassType() == bonusDamageAgainst)
	{
		damage = (unitOffense->getBaseDamage(2.2) - unitDefense[unitNumber]->getClassVit()) * 1.5;
	}
	else
	{
		damage = unitOffense->getBaseDamage(2.2) - unitDefense[unitNumber]->getClassVit();
	}
	cout << endl;
	cout << "Used Assassinate on " << unitDefense[unitNumber]->getClassName() << endl;
	unitDefense[unitNumber]->setClassHealth(-1 * damage);
}
