#pragma once
#include "Class.h"
#include <iostream>

using namespace std;

class ClassMage : public Class
{
public:
	ClassMage();
	~ClassMage();

	void attackEnemy(vector<Class*> enemy, vector<Class*> player);
};

