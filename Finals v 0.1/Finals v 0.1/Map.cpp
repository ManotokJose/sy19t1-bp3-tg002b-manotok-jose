#include "Map.h"

Map::Map()
{
}


Map::~Map()
{
}

void Map::gameStart()
{
	mapPlayer = new Player();
	area.push_back(new Location(mapPlayer->playerPosX, mapPlayer->playerPosY));
	while (mapPlayer->playerHealth > 0)
	{
		areaMove();
		if (checkArea() == false)
		{
			generateNewArea();
		}
		area[currentLocationID]->locationAmbush();
		areaEvent();
	}
}

void Map::generateNewArea()
{
	area.push_back(new Location(mapPlayer->playerPosX, mapPlayer->playerPosY));
	areaCount++;
	currentLocationID = areaCount;
}

void Map::areaMove()
{
	char playerInput;

	area[currentLocationID]->displayLocation();
	cout << "W A S D to move" << endl;
	cout << "T to two hand main weapon" << endl;
	cout << "R to rest" << endl;
	cout << "V to view stats" << endl;

	playerInput = _getch();

	if (playerInput == 'w' || playerInput == 'W')
	{
		mapPlayer->playerPosY++;
	}

	if (playerInput == 'a' || playerInput == 'A')
	{
		mapPlayer->playerPosX--;
	}

	if (playerInput == 's' || playerInput == 'S')
	{
		mapPlayer->playerPosY--;
	}

	if (playerInput == 'd' || playerInput == 'D')
	{
		mapPlayer->playerPosX++;
	}

	if (playerInput == 't' || playerInput == 'T')
	{
		mapPlayer->isTwoHanding = true;
	}

	if (playerInput == 'r' || playerInput == 'R')
	{
		mapPlayer->playerRest();
	}

	if (playerInput == 'v' || playerInput == 'V')
	{
		mapPlayer->displayStats();
	}
}

void Map::areaEvent()
{
	switch (area[currentLocationID]->locationType)
	{
	case 1:
		cout << "You dont see anything that catches your eye in this area" << endl;
		break;

	case 2:
		area[currentLocationID]->locationMonster = new Monster(mapPlayer->playerLevel);
		playerMonsterFight();
		break;

	case 3:
		enterShop();
		break;
	}

}

void Map::enterShop()
{
	char playerInput;

	cout << "You see a shop, would you like to enter?" << endl;
	cout << "y to enter" << endl;
	cout << "type anything else to exit" << endl;
	playerInput = _getch();

	if (playerInput == 'y' || playerInput == 'Y')
	{
		if (area[currentLocationID]->locationShop->checkForMoney() == false)
		{
			cout << "come back when you have more money, yelled the shopkeeper" << endl;
		}
		else
		{
			area[currentLocationID]->locationShop->displayWares();
		}
	}
}

void Map::playerMonsterFight()
{
	int playerInput;
	area[currentLocationID]->locationMonster->monsterLevel = mapPlayer->playerLevel;
	cout << "A " << area[currentLocationID]->locationMonster->monsterName << " appeared!" << endl;
	cout << "What will you do?" << endl;
	
	while (mapPlayer->playerHealth > 0 && area[currentLocationID]->locationMonster->monsterHealth > 0)
	{
		system("cls");
		mapPlayer->displayStats();
		cout << endl;
		cout << area[currentLocationID]->locationMonster->monsterName << " Hp: " << area[currentLocationID]->locationMonster->monsterHealth << "/" << area[currentLocationID]->locationMonster->monsterMaxHealth << endl;
		cout << area[currentLocationID]->locationMonster->monsterName << " total Atk: " << area[currentLocationID]->locationMonster->getDamage() << endl;
		cout << area[currentLocationID]->locationMonster->monsterName << " total Def: " << area[currentLocationID]->locationMonster->getArmour() << endl;
		cout << endl;
		cout << "[1] Fight" << endl;
		cout << "[2] Run" << endl;
		cin >> playerInput;
		if (playerInput == 1)
		{
			playerAreaFight();
			monsterAreaFight();
		}
		else
		{
			if (randomNumber(100, 1) <= 25)
			{
				cout << "You ran away!" << endl;
				area[currentLocationID]->locationMonster->~Monster();
				area[currentLocationID]->locationType = 1;
				break;
			}
			else
			{
				cout << "The " << area[currentLocationID]->locationMonster->monsterName << " caught you!" << endl;
			}
		}
	}
	if (mapPlayer->playerHealth <= 0)
	{
		mapPlayer->~Player();
	}
	else
	{
		cout << "You won!" << endl;
		mapPlayer->playerExp += area[currentLocationID]->locationMonster->monsterExp;
		area[currentLocationID]->locationMonster->~Monster();
		area[currentLocationID]->locationType = 1;
		mapPlayer->playerGold += randomNumber(50, 1);
		if (checkPlayerLevel() == true)
		{
			mapPlayer->levelUp();
		}
	}
}

bool Map::checkPlayerLevel()
{
	if (mapPlayer->playerExp >= mapPlayer->reqExp)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Map::checkArea()
{
	for (int i = 0; i < areaCount; i++)
	{
		if ((area[i]->locationX == mapPlayer->playerPosX) && (area[i]->locationY == mapPlayer->playerPosY))
		{
			area[i]->hasBeenSeen = true;
			currentLocationID = i;
			return true;
		}
	}
	return false;
}

void Map::monsterAreaFight()
{
	int hitRate = (mapPlayer->playerDexterity / area[currentLocationID]->locationMonster->monsterAgility) * 100;
	if (hitRate > 80)
	{
		hitRate = 80;
	}
	if (hitRate < 20)
	{
		hitRate = 20;
	}

	if (randomNumber(100, 1) < hitRate)
	{
		mapPlayer->playerHealth -= (area[currentLocationID]->locationMonster->getDamage() - mapPlayer->getArmour());
	}
	else
	{
		cout << "The " << area[currentLocationID]->locationMonster->monsterName << " missed" << endl;
	}
}

void Map::playerAreaFight()
{
	int hitRate = (area[currentLocationID]->locationMonster->monsterDexterity / mapPlayer->playerAgility) * 100;
	if (hitRate > 80)
	{
		hitRate = 80;
	}
	if (hitRate < 20)
	{
		hitRate = 20;
	}

	if (randomNumber(100, 1) < hitRate)
	{
		area[currentLocationID]->locationMonster->monsterHealth -= (mapPlayer->getDamage() - area[areaCount]->locationMonster->getArmour());
	}
	else
	{
		cout << "You missed!" << endl;
	}
}

int Map::randomNumber(int max, int min)
{
	return rand() % max + min;
}

