#include "HealthPotion.h"


HealthPotion::HealthPotion(Character* player)
{
	player->setHealthPoints();
	setPlayerHealth(player);
}

HealthPotion::~HealthPotion()
{
}

//so i have polymorphism
void HealthPotion::setPlayerHealth(Character* player)
{
	player->setPlayerHealth(30);
}
