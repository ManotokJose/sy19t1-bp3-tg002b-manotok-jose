#pragma once
#include <string>
#include <vector>

using namespace std;

class Skill;
class Class
{
public:
	Class();
	~Class();
	
	virtual void attackEnemy(vector<Class*> enemy, vector<Class*> player);

	float hitRate(Class* enemy);
	int getBaseDamage(float coEff);
	int getClassLevel();
	int getHealth();
	int getMaxHealth();
	int getAgi();
	int getClassDex();
	int getClassVit();
	int getUnitNumber();

	bool checkIfDead();

	void displayStats();
	bool isPlayerUnit();
	string getClassName();
	string getClassType();
	void setClassName();
	void setClassHealth(int number);
	void setPlayerUnit(bool isPlayerUnit);
	void setUnitNumber(int number);
	void setClassMp(int number);
	void regenMana();

	void overrideHealth(int number);
	void overrideMp(int number);

protected:
	int classHealth;
	int classMaxHealth;

	bool playerUnit;

	string classType;
	string className;
	string strongAgainst;

	int classPow;
	int classLevel;
	int classAgi;
	int classDex;
	int classVit;
	int unitNumber;
	int classMaxMp;
	int classMp;
	int moneyReq;
	int manaRegen;

	int randomNumber(int max, int min);
};

