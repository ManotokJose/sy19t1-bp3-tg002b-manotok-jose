#include "Class.h"
#include "Assassinate.h"
#include "SkillBasicAttack.h"
#include <iostream>

using namespace std;

Class::Class()
{
}


Class::~Class()
{
}

int Class::getBaseDamage(float coEff)
{
	int maxPow = classPow * 1.2;
	int randomPow = rand() % (maxPow - 1) + classPow;
	return randomPow * coEff;
}

int Class::getClassLevel()
{
	return 0;
}

int Class::getHealth()
{
	return classHealth;
}

int Class::getMaxHealth()
{
	return classMaxHealth;
}

int Class::getAgi()
{
	return classAgi;
}

int Class::getClassDex()
{
	return classDex;
}

int Class::getClassVit()
{
	return classVit;
}

int Class::getUnitNumber()
{
	return unitNumber;
}

bool Class::checkIfDead()
{
	if (classHealth <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Class::displayStats()
{
	if (checkIfDead())
	{
		cout << "[DEAD]" << endl;
	}
	else
	{
		cout << "Health: " << classHealth << "/" << classMaxHealth << endl;
		cout << "Power: " << classPow << "   Agility: " << classAgi << endl;
		cout << "Vitality: " << classVit << "   Dexterity: " << classDex << endl;
	}
}

bool Class::isPlayerUnit()
{
	return playerUnit;
}

string Class::getClassName()
{
	return className;
}

string Class::getClassType()
{
	return classType;
}

void Class::setClassName()
{
	string name;
	cout << "Name of this character: ";
	cin >> name;
	className = name;
}

void Class::setClassHealth(int number)
{
	classHealth += number;
}

void Class::setPlayerUnit(bool isPlayerUnit)
{
	playerUnit = isPlayerUnit;
}

void Class::setUnitNumber(int number)
{
	unitNumber = number;
}

void Class::setClassMp(int number)
{
	classMp += number;
	if (classMp > classMaxMp)
	{
		overrideMp(classMaxMp);
	}
}

void Class::regenMana()
{
	if (classMp > classMaxMp)
	{
		classMp += manaRegen;
		if (classMp > classMaxMp)
		{
			classMp = classMaxMp;
		}
	}
}

void Class::overrideHealth(int number)
{
	classHealth = number;
}

void Class::overrideMp(int number)
{
	classMp = number;
}

int Class::randomNumber(int max, int min)
{
	return rand() % max + min;
}

void Class::attackEnemy(vector<Class*> player, vector<Class*> enemy)
{
}

float Class::hitRate(Class* enemy)
{
	float hitRate = (classDex / enemy->classAgi) * 100;
	if (hitRate < 20)
	{
		return 20;
	}
	else if (hitRate > 80)
	{
		return 80;
	}
	else
	{
		return hitRate;
	}
}
