#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Character.h"
#include "Skill.h"
#include "CritSkill.h"
#include "SyphonSkill.h"
#include "VenganceSkill.h"

using namespace std;

class DopelSkill : public Skill
{
public:
	DopelSkill();
	~DopelSkill();

	void doSkillAction(Character* player, Character* enemy);
};

