#pragma once
#include <string>
#include "Class.h"
#include "Skill.h"

using namespace std;

class Heal : public Skill
{
public:
	Heal();
	~Heal();

	void classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst);
};

