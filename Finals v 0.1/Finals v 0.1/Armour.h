#pragma once
#include <string>

using namespace std;

class Armour
{
public:
	Armour(int slot);
	~Armour();

	string armourName;
	string armourType;
	string armourPrefix;

	int armourDamage;
	int armourPrice;

private:
	int randomNumber(int max, int min);
};

