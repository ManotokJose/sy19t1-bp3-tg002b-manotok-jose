#pragma once
#include <string>
#include "Class.h"
#include "Skill.h"

using namespace std;

class Shockwave : public Skill
{
public:
	Shockwave();
	~Shockwave();

	void classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst);
};

