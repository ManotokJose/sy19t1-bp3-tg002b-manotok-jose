#pragma once
#include <string>
#include "Class.h"
#include "Skill.h"

using namespace std;

class SkillBasicAttack : public Skill
{
public:
	SkillBasicAttack();
	~SkillBasicAttack();

	void classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst);
};


