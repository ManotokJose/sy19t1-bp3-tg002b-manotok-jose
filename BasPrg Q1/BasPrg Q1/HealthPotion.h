#pragma once
#include "Item.h"
#include <iostream>

using namespace std;

class Character;
class HealthPotion : public Item
{
public:
	HealthPotion(Character* player);
	~HealthPotion();

	void setPlayerHealth(Character* player);
private:
};

