#include "Heal.h"
#include <iostream>

using namespace std;

Heal::Heal()
{
	skillName = "Heal";
	costMP = 70;
}


Heal::~Heal()
{
}

void Heal::classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst)
{
	int unitNumber = 0;

	for (int i = 0; i < player.size(); i++)
	{
		if (player[unitNumber]->getHealth() <= 0)
		{
			unitNumber++;
		}

		if (player[i]->checkIfDead() == false)
		{
			if (player[i]->getHealth() < player[unitNumber]->getHealth())
			{
				unitNumber = i;
			}
		}
	}

	cout << "Used Heal on " << player[unitNumber]->getClassName() << endl << endl;
	player[unitNumber]->setClassHealth(player[unitNumber]->getMaxHealth() * .3);

	if (player[unitNumber]->getHealth() > player[unitNumber]->getMaxHealth())
	{
		player[unitNumber]->overrideHealth(player[unitNumber]->getMaxHealth());
	}
}
