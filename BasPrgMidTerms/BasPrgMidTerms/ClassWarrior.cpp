#include "ClassWarrior.h"
#include "SkillBasicAttack.h"
#include "Shockwave.h"


ClassWarrior::ClassWarrior()
{
	className = "Enemy Warrior";
	strongAgainst = "Assassin";
	classType = "Warrior";
	classLevel = 1;
	classMp = 100;
	classMaxMp = classMp;
	classPow = 10;
	classHealth = 50;
	classMaxHealth = classHealth;
	classAgi = 1;
	classDex = 10;
	classVit = 15;

}


ClassWarrior::~ClassWarrior()
{
}

void ClassWarrior::attackEnemy(vector<Class*> player, vector<Class*> enemy)
{
	vector<Skill*> Skills;
	Skills.push_back(new SkillBasicAttack);
	Skills.push_back(new Shockwave);

	int skillNumber;
	if (playerUnit == true)
	{
		int playerInput;
		cout << "[1] " << Skills[0]->getSkillName() << endl;
		cout << "[2] " << Skills[1]->getSkillName() << endl;
		cout << "Input Skill Number: ";
		cin >> skillNumber;
		skillNumber--;
	}
	else
	{
		skillNumber = rand() % 2 + 0;
		cout << skillNumber;
	}

	switch (skillNumber)
	{
	case 0:
		cout << "Used " << Skills[0]->getSkillName() << endl;
		break;
	case 1:
		cout << "Used " << Skills[1]->getSkillName() << endl;
		break;
	}

	if (classMp < Skills[skillNumber]->getSkillCost() && skillNumber != 0)
	{
		skillNumber = 0;
		cout << "Not enough MP... resorting to basic attack skill" << endl;
	}

	Skills[skillNumber]->classSkill(this, player, enemy, strongAgainst);
	classMp -= Skills[skillNumber]->getSkillCost();
	regenMana();

	for (int i = 0; i < Skills.size(); i++)
	{
		delete Skills[i];
	}
}
