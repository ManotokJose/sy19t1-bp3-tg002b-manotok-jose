#pragma once
#include "Player.h"
class Game
{
public:
	Game(Player* p);
	~Game();

	//hub (needs to much change ): )
	//void displayHub();
	//void levelTeam();

	//combat
	void gameCombat();
	void sortTurnOrder();
	void displayTeam(vector<Class*> team);
	void checkForDead(Player* player);

	Player* player;
	Player* enemy;

	vector<Class*> turnOrder;

private:
	bool playerWonCombat;
};

