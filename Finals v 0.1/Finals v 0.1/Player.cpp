#include "Player.h"
#include <iostream>


Player::Player()
{
	int playerClass;
	string name;

	playerPosX = 0;
	playerPosY = 0;
	playerExp = 0;

	cout << "Name: ";
	cin >> name;
	cout << "Choose a class: " << endl;
	cout << "[1] Warrior" << endl; //HAtk, MHp, MDef
	cout << "[2] Theif" << endl; //HAgi, MAtk, MDex
	cout << "[3] Crusader" << endl; //VHDef, MHp
	cout << "[4] Executioner" << endl; // LHp, MAtk, MDef
	cout << "[5] Blademaster" << endl; //HDex, MAgi, LHp
	cout << "[6] Hobo" << endl; // set everything to 1
	cout << "Class: ";
	cin >> playerClass;

	//name, health, mhealth, atk, def, agi, dex
	switch (playerClass)
	{
	case 1: //warrior
		setValues(name, 6, 9, 8, 4, 5, "Warrior");
		break;

	case 2: //thief
		setValues(name, 5, 6, 4, 9, 7, "Theif");
		break;

	case 3: //crusader
		setValues(name, 7, 5, 10, 6, 6, "Crusader");
		break;

	case 4: //exec
		setValues(name, 4, 6, 7, 6, 6, "Executioner");
		break;

	case 5: //blade
		setValues(name, 6, 6, 6, 7, 7, "Blademaster");
		break;

	case 6: //hobo
		setValues(name, 1, 1, 1, 1, 1, "Hobo");
		break;
	}
}


Player::~Player()
{
	cout << "GG you ded" << endl;
}

void Player::setValues(string name, int health, int atk, int def, int agi, int dex, string pClass)
{
	playerLevel = 1;
	playerHealth = health;
	playerMaxHealth = playerHealth;
	playerAttack = atk;
	playerDefense = def;
	playerAgility = agi;
	playerDexterity = dex;
	playerLuck = playerLevel * 10;
	playerGold = 10;
	playerClass = pClass;
	playerLuck = randomNumber(10, 1);
	reqExp = playerLevel * 1000;

	playerOffense = new Weapon(false);
	playerOffhand = new Weapon(true);

	playerHelmet = new Armour(1);
	playerChestplate = new Armour(2);
	playerGauntlet = new Armour(3);
	playerLegArmour = new Armour(4);

	isTwoHanding = false;
}

int Player::getDamage()
{
	if (isTwoHanding == true)
	{
		return playerAttack + (playerOffense->weaponDamage * 1.5);
	}
	else
	{
		return playerAttack + playerOffense->weaponDamage + playerOffhand->weaponDamage;
	}
}

int Player::getArmour()
{
	return playerHelmet->armourDamage + playerChestplate->armourDamage + playerGauntlet->armourDamage + playerLegArmour->armourDamage + playerOffhand->damageNegation;
}

void Player::displayStats()
{
	cout << playerClass << " " << playerName << endl;
	cout << "Health: " << playerHealth << "/" << playerMaxHealth << endl;
	cout << "total Atk: " << getDamage() << endl;
	cout << "total Def: " << getArmour() << endl;
	cout << "Agi: " << playerAgility << endl;
	cout << "Dex: " << playerDexterity << endl;
	cout << "Gold: " << playerGold << endl;

	cout << playerOffense->weaponName << endl;
	cout << playerOffhand->weaponDamage << endl;

	cout << playerHelmet->armourName << endl;
	cout << playerChestplate->armourName << endl;
	cout << playerGauntlet->armourName << endl;
	cout << playerLegArmour->armourName << endl;
}

void Player::playerRest()
{
	cout << "You rested" << endl;
	playerHealth = playerMaxHealth;
}

void Player::levelUp()
{
	cout << "you leveled up" << endl;
	playerLevel++;
	playerMaxHealth += randomNumber(3, 0);
	playerHealth = playerMaxHealth;
	playerAttack += randomNumber(3, 0);
	playerDefense += randomNumber(3, 0);
	playerAgility += randomNumber(3, 0);
	playerDexterity += randomNumber(3, 0);
	reqExp = playerLevel * 1000;
}

int Player::randomNumber(int max, int min)
{
	return max, + min;
}

