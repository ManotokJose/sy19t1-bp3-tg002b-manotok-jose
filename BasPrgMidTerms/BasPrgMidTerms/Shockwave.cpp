#include "Shockwave.h"
#include <iostream>

using namespace std;

Shockwave::Shockwave()
{
	skillName = "Shockwave";
	costMP = 60;
}


Shockwave::~Shockwave()
{
}

void Shockwave::classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst)
{
	int damage;
	for (int i = 0; i < unitDefense.size(); i++)
	{
		if (unitDefense[i]->getClassType() == bonusDamageAgainst)
		{
			damage = unitOffense->getBaseDamage(0.9) - unitDefense[i]->getClassVit() * 1.5;
		}
		else
		{	
			damage = unitOffense->getBaseDamage(0.9) - unitDefense[i]->getClassVit();
		}
		cout << "Used Shockwave on " << unitDefense[i]->getClassName() << endl;
		unitDefense[i]->setClassHealth(-1 * damage);
	}
}
