#pragma once
#include <iostream>


using namespace std;

class Character
{
public:
	Character();
	~Character();

	void rollItem();
	void displayEndStats();

	int getPlayerHealth();
	int getPlayerCrystal();
	int getPlayerPoints();

	void setPlayerPoints(int number);
	void setPlayerHealth(int number);
	void setPlayerCrystal(int number);

	// end stats
	void setHealthPoints();
	void setBombPoints();
	void setCrystalPoints();
	void setRPoints();
	void setSRPoints();
	void setSSRPoints();
	
	// misc
	int randomNumber(int max, int min);

private:
	int points;
	int health;
	int crystal;

	// for end stats
	int pointsR;
	int pointsSR;
	int pointsSSR;
	int pointsBomb;
	int pointsHealth;
	int pointsCrystal;
};

