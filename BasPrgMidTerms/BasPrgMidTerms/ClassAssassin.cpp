#include "ClassAssassin.h"
#include "Skill.h"
#include "SkillBasicAttack.h"
#include "Assassinate.h"
#include <iostream>

using namespace std;

ClassAssassin::ClassAssassin()
{
	className = "Enemy Assassin";
	classType = "Assassin";
	strongAgainst = "Mage";
	classLevel = 1;
	classMp = 100;
	classMaxMp = classMp;
	classPow = 10;
	classHealth = 30;
	classMaxHealth = classHealth;
	classAgi = 15;
	classDex = 10;
	classVit = 5;
}


ClassAssassin::~ClassAssassin()
{
}

void ClassAssassin::attackEnemy(vector<Class*> player, vector<Class*> enemy)
{
	vector<Skill*> Skills;
	Skills.push_back(new SkillBasicAttack);
	Skills.push_back(new Assassinate);

	int skillNumber;
	if (playerUnit == true)
	{
		int playerInput;
		cout << "[1] " << Skills[0]->getSkillName() << endl;
		cout << "[2] " << Skills[1]->getSkillName() << endl;
		cout << "Input Skill Number: ";
		cin >> skillNumber;
		skillNumber--;
	}
	else
	{
		skillNumber = rand() % 2 + 0;
	}

	switch (skillNumber)
	{
	case 0:
		cout << "Used " << Skills[0]->getSkillName() << endl;
		break;
	case 1:
		cout << "Used " << Skills[1]->getSkillName() << endl;
		break;
	}

	if (classMp < Skills[skillNumber]->getSkillCost() && skillNumber != 0)
	{
		skillNumber = 0;
		cout << "Not enough MP... resorting to basic attack skill" << endl;
	}

	Skills[skillNumber]->classSkill(this, player, enemy, strongAgainst);
	classMp -= Skills[skillNumber]->getSkillCost();
	regenMana();

	for (int i = 0; i < Skills.size(); i++)
	{
		delete Skills[i];
	}
}
