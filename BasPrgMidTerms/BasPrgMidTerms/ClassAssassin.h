#pragma once
#include "Class.h"
class ClassAssassin : public Class
{
public:
	ClassAssassin();
	~ClassAssassin();

	void attackEnemy(vector<Class*> enemy, vector<Class*> player);
};

