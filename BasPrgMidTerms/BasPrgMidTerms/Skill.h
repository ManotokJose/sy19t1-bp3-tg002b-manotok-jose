#pragma once
#include <string>
#include <vector>
#include "Class.h"

using namespace std;

class Skill
{
public:
	Skill();
	~Skill();

	virtual void classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst);
	string getSkillName();
	int getSkillCost();

protected:
	string skillName;
	int costMP;
	int damageCoEff;
	string effectiveAgainst;
};

