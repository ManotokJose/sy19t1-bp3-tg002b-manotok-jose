#include "SkillBasicAttack.h"
#include <iostream>

using namespace std;

SkillBasicAttack::SkillBasicAttack()
{
	skillName = "Basic Attack";
	costMP = 0;
}


SkillBasicAttack::~SkillBasicAttack()
{
}

void SkillBasicAttack::classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst)
{
	int damage;

	int randomNumber = rand() % unitDefense.size() + 0;
	while (unitDefense[randomNumber]->checkIfDead())
	{
		randomNumber = rand() % unitDefense.size() + 0;
	}

	if (unitDefense[randomNumber]->getClassType() == bonusDamageAgainst)
	{
		damage = (unitOffense->getBaseDamage(1.0) - unitDefense[randomNumber]->getClassVit()) * 1.5;
	}
	else
	{
		damage = unitOffense->getBaseDamage(1.0) - unitDefense[randomNumber]->getClassVit();
	}

	if (rand() % 100 + 1 <= 20)
	{
		damage = damage * 1.2;
	}

	if (damage <= 0)
	{
		damage = 1;
	}
	cout << "Hit " << unitDefense[randomNumber]->getClassName() << endl;
	unitDefense[randomNumber]->setClassHealth(-1 * damage);
}
