#pragma once
#include <string>
#include "Class.h"
#include "Skill.h"

using namespace std;

class Assassinate : public Skill
{
public:
	Assassinate();
	~Assassinate();

	void classSkill(Class* unitOffense, vector<Class*> player, vector<Class*> unitDefense, string bonusDamageAgainst);
};

