#pragma once
#include "Weapon.h"
#include <string>
#include <iostream>

using namespace std;

class Character
{
public:
	Character();
	~Character();

	int health;
	int mp;
	int maxMp;

	Weapon* characterWeapon;

	int getDamage();

	void attackCharacter(Character* enemy);
	bool mpCheck(int cmp);
	int randomNumber();
	int characterManaRegen();
};

