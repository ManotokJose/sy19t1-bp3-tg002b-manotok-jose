#pragma once
#include "Class.h"
#include <iostream>

using namespace std;

class ClassWarrior : public Class
{
public:
	ClassWarrior();
	~ClassWarrior();

	void attackEnemy(vector<Class*> enemy, vector<Class*> player);
};

