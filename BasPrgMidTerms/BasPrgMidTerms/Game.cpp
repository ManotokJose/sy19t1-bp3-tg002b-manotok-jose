#include "Game.h"
#include <iostream>
#include <algorithm>

using namespace std;

Game::Game(Player* p)
{
	Player* e = new Player(true);

	player = p;
	enemy = e;
}


Game::~Game()
{
	for (int i = 0; i < turnOrder.size(); i++)
	{
		delete turnOrder[i];
	}
}

void Game::gameCombat()
{
	system("cls");

	for (int i = 0; i < 3; i++)
	{
		turnOrder.push_back(enemy->playerTeam[i]);
	}
	for (int i = 0; i < 3; i++)
	{
		turnOrder.push_back(player->playerTeam[i]);
	}
	sortTurnOrder();

	while (player->isTeamDead() == false && enemy->isTeamDead() == false)
	{
		system("cls");
		cout << "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+==+=+=" << endl;
		cout << "Player team" << endl;
		displayTeam(player->playerTeam);
		cout << "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+==+=+=" << endl;
		cout << "Enemy team" << endl;
		displayTeam(enemy->playerTeam);
		cout << "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+==+=+=" << endl;

		if (turnOrder[0]->checkIfDead() == false)
		{
			cout << turnOrder[0]->getClassName() << endl;
			if (turnOrder[0]->isPlayerUnit() == true)
			{
				cout << "player turn" << endl;
				turnOrder[0]->attackEnemy(player->playerTeam, enemy->playerTeam);
			}
			else
			{
				cout << "enemy turn" << endl;
				turnOrder[0]->attackEnemy(enemy->playerTeam, player->playerTeam);
			}
		}
		turnOrder.push_back(turnOrder[0]);
		turnOrder.erase(turnOrder.begin() + 0);

		system("pause");
	}

	system("cls");
	if (player->isTeamDead() == true)
	{
		cout << "You lost" << endl;
	}
	else
	{
		cout << "You won" << endl;
	}

}

void Game::sortTurnOrder() //code from https://mathbits.com/MathBits/CompSci/Arrays/Bubble.htm
{
	int i;
	int j;
	int loopCount = 1;
	Class* temp;

	for (int i = 1; (i <= turnOrder.size()) && loopCount; i++)
	{
		loopCount = 0;
		for (int j = 0; j < (turnOrder.size() - 1); j++)
		{
			if (turnOrder[j + 1]->getAgi() > turnOrder[j]->getAgi())
			{
				temp = turnOrder[j];
				turnOrder[j] = turnOrder[j + 1];
				turnOrder[j + 1] = temp;
				loopCount = 1;
			}
		}
	}
}

void Game::displayTeam(vector<Class*> team)
{
	for (int i = 0; i < team.size(); i++)
	{
		cout << team[i]->getClassName() << endl;
		team[i]->displayStats();
		cout << endl;
	}
}

void Game::checkForDead(Player * player)
{
	for (int i = 0; i < player->playerTeam.size(); i++)
	{
		if (player->playerTeam[i]->checkIfDead())
		{
			player->playerTeam.erase(player->playerTeam.begin() + i);
		}
	}
}


