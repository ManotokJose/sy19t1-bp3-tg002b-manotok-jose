#include "CritSkill.h"



CritSkill::CritSkill()
{
}


CritSkill::~CritSkill()
{
}

void CritSkill::doSkillAction(Character* player, Character* enemy)
{
	cout << player->getName() << ": " << "Critical Attack" << endl;
	enemy->addHealth(-1 * (player->getCharacterDamage() * 2));
}
